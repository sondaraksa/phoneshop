var express         = require('express')
var app             = express();
var bcrypt          = require('bcrypt-nodejs');
var passport        = require('../passport');


app.post('/login', passport.authenticate('local', {
    failureRedirect: '/login',
    successRedirect: '/',
}), function (req, res) {
    res.send('hey')
})

app.post('/register',function(req, res){
        req.assert('username', 'username is required').notEmpty();
        req.assert('password', 'password is required').notEmpty();
        var errors = req.validationErrors();
        if( !errors ){
            var user = {
                username: req.sanitize('username').escape().trim(),
                password: bcrypt.hashSync(req.sanitize('password').escape().trim(),null,null)
            }
            req.getConnection(function(error, conn) {
                conn.query("SELECT * FROM phoneshop.users where username = ? ", req.sanitize('username').escape().trim(),  function(errr,rows){
                    if(errr) return console.log('error nov nis');
                    if(rows.length){
                        console.log('email is already taken');
                        return res.render('user/register');
                    }else{
                        req.getConnection(function(error1, conn) {
                            conn.query('INSERT INTO users SET ?', user, function(err, result) {
                                //if(err) throw err
                                if (err) {
                                    req.flash('error', err)
                                    res.render('user/register');
                                } else {                
                                        req.flash('success', 'Data added successfully!')
                                        
                                        // render to views/user/index.ejs
                                        res.render('index', {title: 'successfully register'});
                                    }
                            })
                        })
                    }
                })
            })
                
        }else{
            console.log('error here');
            var error_msg = ''
            errors.forEach(function(error) {
                error_msg += error.msg + '<br>'
            })                
            req.flash('error', error_msg)        
            
            /**
             * Using req.body.name 
             * because req.param('name') is deprecated
             */ 
            res.render('user/register');
        }
})

module.exports=app;

