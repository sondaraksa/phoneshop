var express = require('express')
var app     = express()

    app.get('/',function(req, res, next) {
        res.render('index', {title: 'My Node.js Application'});
    
    })

    app.get('/login',function(req, res , next){
        res.render('user/login');
    })
  
    app.get('/register',isLoggedIn,function(req, res, next){
        res.render('user/register');
    })

    app.get('/logout', function (req, res) {
        req.logout()
        res.redirect('/')
    })


    function isLoggedIn(req, res, next){
    if(req.isAuthenticated())  return next();
       res.redirect('/login');
    }


module.exports = app;