var passport        = require("passport");
var bcrypt          = require('bcrypt-nodejs');
var LocalStrategy   = require("passport-local").Strategy;

    passport.initialize();
    passport.serializeUser(function(user, done) {
        done(null, user);
    });
    passport.deserializeUser(function(user, done) {
        done(null, user);
    });

    passport.use(new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true,
        session: false
    },
    function(req, username, password, done) {
        req.getConnection(function(err,conn){
            conn.query("SELECT * FROM phoneshop.users where username = ? ", username,function(error, rows){
                if(error){
                    return done(err);
                } 
                if(!rows.length){ 
                    return  done(null, false);
                }else{
                    bcrypt.compare(password, rows[0].password,function(err, res){
                        if(res===true){
                            var user = {
                                username: rows[0].username,
                                password: rows[0].password
                            }
                            return done(null, user);
                        }else{
                            return done(err);
                        }
                        
                    });
                    
                }
            })
        })
    }
    ));

module.exports=passport;

