var express             = require('express');
var mysql               = require('mysql');
var path                = require('path');
var myConnection        = require('express-myconnection');
var config              = require('./config');
var expressValidator    = require('express-validator');
var bodyParser          = require('body-parser');
var methodOverride      = require('method-override');
var flash               = require('express-flash');
var cookieParser        = require('cookie-parser');
var session             = require('express-session');
var app                 = express();

var passport            = require('./passport');

var dbOptions           = {
    host:      config.database.host,
    user:       config.database.user,
    password: config.database.password,
    port:       config.database.port, 
    database: config.database.db
}
var index   = require('./routes/index');
var auth    = require('./routes/auth');

app.use("/public", express.static(path.join(__dirname, '/public')));
app.use("/bootstrap", express.static(path.join(__dirname, '/node_modules/bootstrap/dist')));
app.use("/jquery", express.static(path.join(__dirname, '/node_modules/jquery/dist')));

app.use(myConnection(mysql, dbOptions, 'pool'));
app.set('view engine', 'ejs');

app.use(expressValidator());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(methodOverride(function (req, res) {
  if (req.body && typeof req.body === 'object' && '_method' in req.body) {
    // look in urlencoded POST bodies and delete it
    var method = req.body._method
    delete req.body._method
    return method
  }
}));

app.use(cookieParser('keyboard cat'));
app.use(session({ 
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 60000 }
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
 
app.use('/', index);
app.use('/auth',auth);

app.listen(3000, function(){
    console.log('Server running at port 3000: http://127.0.0.1:3000')
});
